import React from "react";
import CIcon from "@coreui/icons-react";
import { NavLink } from "react-router-dom";
import connect from "../assets/icons/Connect.svg";
console.log(connect);

const _nav = [
  {
    _tag: "CSidebarNavItem",
    name: "Dashboard",
    to: "/dashboard",
    icon: (
      <span
        className="iconify c-sidebar-nav-icon"
        data-icon="ant-design:stock-outlined"
        data-inline="false"
        style={{ height: "25px", width: "25px" }}
      ></span>
    ),
    // badge: {
    //   color: "info",
    //   text: "NEW",
    // },
  },
  {
    _tag: "CSidebarNavTitle",
    _children: ["CONNECT"],
  },
  {
    _tag: "CSidebarNavItem",
    name: "Connect",
    to: "/projects/Projects",
    icon: (
      <span
        className="iconify c-sidebar-nav-icon"
        data-icon="grommet-icons:connect"
        data-inline="false"
        style={{ height: "25px", width: "25px" }}
      ></span>
    ),
  },
  {
    _tag: "CSidebarNavTitle",
    _children: ["CALIBRATE"],
  },
  {
    _tag: "CSidebarNavItem",
    name: "Projects",
    to: "/theme/colors",
    icon: (
      <i
        className="iconify c-sidebar-nav-icon"
        data-icon="ic:outline-business"
        data-inline="false"
        style={{ height: "25px", width: "25px" }}
      ></i>
    ),
  },
  {
    _tag: "CSidebarNavItem",
    name: "Manpower",
    to: "/theme/colors",
    icon: (
      <span
        className="iconify c-sidebar-nav-icon"
        data-icon="grommet-icons:user-worker"
        data-inline="false"
        style={{ height: "25px", width: "25px" }}
      ></span>
    ),
  },
  {
    _tag: "CSidebarNavItem",
    name: "Inventory",
    to: "/theme/colors",
    icon: (
      <span
        className="iconify c-sidebar-nav-icon"
        data-icon="mdi:warehouse"
        data-inline="false"
        style={{ height: "25px", width: "25px" }}
      ></span>
    ),
  },
  {
    _tag: "CSidebarNavItem",
    name: "Equipment",
    to: "/theme/colors",
    icon: (
      <span
        className="iconify c-sidebar-nav-icon"
        data-icon="tabler:backhoe"
        data-inline="false"
        style={{ height: "25px", width: "25px" }}
      ></span>
    ),
  },
  {
    _tag: "CSidebarNavTitle",
    _children: ["LEARN"],
  },
  {
    _tag: "CSidebarNavItem",
    name: "Insights",
    to: "/theme/colors",
    icon: (
      <span
        className="iconify c-sidebar-nav-icon"
        data-icon="ant-design:stock-outlined"
        data-inline="false"
        style={{ height: "25px", width: "25px" }}
      ></span>
    ),
  },
];

export default _nav;
