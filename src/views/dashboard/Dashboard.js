import React, { lazy } from "react";
import {
  CButton,
  CCard,
  CCardBody,
  CCardFooter,
  CCardHeader,
  CCol,
  CRow,
  CLink,
  CBadge,
  CListGroupItem,
  CWidgetBrand,
  CWidgetIcon,
  CProgress,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import { DocsLink } from "src/reusable";
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import Project from "../../assets/img/project.jpg";
import { CircularProgressbarWithChildren } from "react-circular-progressbar";
import "react-circular-progressbar/dist/styles.css";
import "./Dashboard.css";
import ChartLineSimple from "../charts/ChartLineSimple";

const Dashboard = () => {
  const responsive = {
    superLargeDesktop: {
      // the naming can be any, depends on you.
      breakpoint: { max: 4000, min: 3000 },
      items: 5,
    },
    desktop: {
      breakpoint: { max: 3000, min: 1024 },
      items: 3,
    },
    tablet: {
      breakpoint: { max: 1024, min: 464 },
      items: 2,
    },
    mobile: {
      breakpoint: { max: 464, min: 0 },
      items: 1,
    },
  };

  const ProjectStatus = 90;
  const TimeLine = 90;
  const RemainingBudget = 30;
  const Risk = 10;

  const Projects = [
    { Id: 1, ProjectName: "Project 1", ProfitMargin: "50" },
    { Id: 2, ProjectName: "Project 2", ProfitMargin: "50" },
    { Id: 3, ProjectName: "Project 3", ProfitMargin: "50" },
    { Id: 4, ProjectName: "Project 4", ProfitMargin: "50" },
    { Id: 5, ProjectName: "Project 5", ProfitMargin: "50" },
    { Id: 6, ProjectName: "Project 6", ProfitMargin: "50" },
    { Id: 7, ProjectName: "Project 7", ProfitMargin: "50" },
    { Id: 8, ProjectName: "Project 8", ProfitMargin: "50" },
    { Id: 9, ProjectName: "Project 9", ProfitMargin: "50" },
    { Id: 10, ProjectName: "Project 10", ProfitMargin: "50" },
    { Id: 11, ProjectName: "Project 11", ProfitMargin: "50" },
    { Id: 12, ProjectName: "Project 12", ProfitMargin: "50" },
    { Id: 13, ProjectName: "Project 13", ProfitMargin: "50" },
    { Id: 14, ProjectName: "Project 14", ProfitMargin: "50" },
    { Id: 15, ProjectName: "Project 15", ProfitMargin: "50" },
    { Id: 16, ProjectName: "Project 16", ProfitMargin: "50" },
    { Id: 17, ProjectName: "Project 17", ProfitMargin: "30" },
    { Id: 18, ProjectName: "Project 18", ProfitMargin: "29" },
    { Id: 19, ProjectName: "Project 20", ProfitMargin: "29" },
    { Id: 20, ProjectName: "Project 21", ProfitMargin: "29" },
    { Id: 21, ProjectName: "Project 22", ProfitMargin: "29" },
    { Id: 22, ProjectName: "Project 23", ProfitMargin: "29" },
    { Id: 23, ProjectName: "Project 24", ProfitMargin: "25" },
    { Id: 24, ProjectName: "Project 25", ProfitMargin: "27" },
    { Id: 25, ProjectName: "Project 26", ProfitMargin: "25" },
    { Id: 26, ProjectName: "Project 27", ProfitMargin: "28" },
  ];

  const TopList = Projects.map((proj) => {
    if (proj.ProfitMargin >= 30) {
      return (
        <CListGroupItem className="justify-content-between " key={proj.Id}>
          {proj.ProjectName}
          <CBadge className="float-right" shape="pill" color="success">
            <span style={{ fontSize: "12px", fontWeight: "700" }}>
              {proj.ProfitMargin}%
            </span>
          </CBadge>
        </CListGroupItem>
      );
    }
  });

  const BottomList = Projects.map((proj) => {
    if (proj.ProfitMargin < 30) {
      return (
        <CListGroupItem
          accent="danger"
          color="danger"
          className="justify-content-between list_card"
          key={proj.Id}
        >
          {proj.ProjectName}
          <CBadge className="float-right" shape="pill" color="danger">
            <span style={{ fontSize: "12px", fontWeight: "700" }}>
              {proj.ProfitMargin}%
            </span>
          </CBadge>
        </CListGroupItem>
      );
    }
  });

  return (
    <>
      <CRow>
        <CCol xs="12" sm="4" md="4" xl="3">
          <div className="card shadow">
            <div className="card-header content-center text-white  bg-primary_dark">
              <div>
                <div className="project_val">250</div>
                <div className="project_label">ALL PROJECTS</div>
              </div>
            </div>
            <div
              className="card-body row text-center"
              style={{ height: "75px" }}
            >
              <div className="col">
                <div className="text-value-lg">205</div>
                <div className="text-uppercase small">ON TIME</div>
              </div>
              <div className="c-vr"></div>
              <div className="col">
                <div className="text-value-lg">45</div>
                <div className="text-uppercase text-muted small">DELAYED</div>
              </div>
            </div>
          </div>
        </CCol>
        <CCol xs="12" sm="4" md="3" xl="3">
          <div className="card success shadow">
            <div className="card-body d-flex align-items-center p-0">
              <div className="mr-3 text-white bg-success p-4">
                <span
                  className="iconify"
                  data-icon="grommet-icons:money"
                  data-inline="false"
                ></span>
              </div>
              <div>
                <div className="text-value text-value-lg">$1.999,50</div>
                <div className="text-muted text-uppercase font-weight-bold small">
                  COLLECTABLES
                </div>
              </div>
            </div>
            <footer className="card-footer px-3 py-2 card-footer">
              <a
                href="https://coreui.io/"
                className="font-weight-bold font-xs btn-block text-muted"
                rel="noopener norefferer"
                target="_blank"
              >
                View more
              </a>
            </footer>
          </div>
        </CCol>
        <CCol xs="12" sm="4" md="3" xl="3">
          <div className="card success shadow">
            <div className="card-body d-flex align-items-center p-0">
              <div className="mr-3 text-white bg-success p-4">
                <span
                  className="iconify"
                  data-icon="grommet-icons:money"
                  data-inline="false"
                  // style={{ height: "90px", width: "25px" }}
                ></span>
              </div>
              <div>
                <div className="text-value text-value-lg">$1.999,50</div>
                <div className="text-muted text-uppercase font-weight-bold small">
                  COLLECTABLES
                </div>
              </div>
            </div>
            <footer className="card-footer px-3 py-2 card-footer">
              <a
                href="https://coreui.io/"
                className="font-weight-bold font-xs btn-block text-muted"
                rel="noopener norefferer"
                target="_blank"
              >
                View more
              </a>
            </footer>
          </div>
        </CCol>
        <CCol xs="12" sm="4" md="2" xl="2">
          <CCard className="text-black shadow">
            <CCardBody>
              <h4 className="pull-left">45%</h4>
              <span style={{ fontSize: "12px", fontWeight: "600" }}>
                PROFIT MARGIN
              </span>
              <CProgress color="success" value={45} size="xs" />
              <span
                style={{
                  fontSize: "10px",
                  letterSpacing: "0px",
                  textAlign: "left",
                  lineHeight: "normal",
                }}
              >
                Acceptable profit margin explained in this section. This will
                serve as a reminder to the person reading.
              </span>
            </CCardBody>
          </CCard>
        </CCol>
        <CCol xs="12" sm="4" md="2" xl="2">
          <CCard className="text-black shadow">
            <CCardBody>
              <h4 className="pull-left">45%</h4>
              <span style={{ fontSize: "12px", fontWeight: "600" }}>
                RISK VALUE
              </span>
              <span
                style={{
                  fontSize: "10px",
                  letterSpacing: "0px",
                  textAlign: "left",
                  lineHeight: "normal",
                }}
              >
                Acceptable profit margin explained in this section. This will
                serve as a reminder to the person reading.
              </span>
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
      <CRow>
        <CCol sm="6" md="9">
          <CCard>
            <CCardHeader className="text-center">
              <h2>TOP 5 EARNING PROJECTS</h2>
            </CCardHeader>
            <CCardBody>
              {/* <div style={{ marginBottom: "10px" }}>
                <span className="badge_green">PROJECT STATUS</span>
                <span className="badge_blue">TINELINE</span>
                <span className="badge_violet">REMAINING BUDGET</span>
                <span className="badge_orange">RISK</span>
              </div> */}
              <Carousel responsive={responsive}>
                <div className="card_container">
                  <img
                    src={Project}
                    alt="Avatar"
                    style={{
                      width: "100%",
                      height: "55%",
                      borderTopLeftRadius: "20px",
                      borderTopRightRadius: "20px",
                    }}
                  />
                  <div className="container_box">
                    <div style={{ width: 60, height: 60 }}>
                      <span className="title">PROJECT STATS</span>
                      <CircularProgressbarWithChildren
                        value={ProjectStatus}
                        text={`${ProjectStatus}%`}
                        styles={{
                          path: {
                            stroke: "#54c2a2",
                            strokeLinecap: "butt",
                            transition: "stroke-dashoffset 0.5s ease 0s",
                            transformOrigin: "center center",
                          },
                          trail: {
                            stroke: "#b0ebda",
                            strokeLinecap: "butt",
                            transform: "rotate(0.25turn)",
                            transformOrigin: "center center",
                          },
                          text: {
                            fill: "black",
                            fontSize: "20px",
                            fontWeight: "700",
                          },
                          background: {
                            fill: "#54c2a2",
                          },
                        }}
                      ></CircularProgressbarWithChildren>
                    </div>
                    <div style={{ width: 60, height: 60 }}>
                      <span className="title">TIMELINE</span>
                      <CircularProgressbarWithChildren
                        value={TimeLine}
                        text={`${TimeLine}%`}
                        styles={{
                          path: {
                            stroke: "#4ccee8",
                            strokeLinecap: "butt",
                            transition: "stroke-dashoffset 0.5s ease 0s",
                            transformOrigin: "center center",
                          },
                          trail: {
                            stroke: "#bbe5ed",
                            strokeLinecap: "butt",
                            transform: "rotate(0.25turn)",
                            transformOrigin: "center center",
                          },
                          text: {
                            fill: "black",
                            fontSize: "20px",
                            fontWeight: "700",
                          },
                          background: {
                            fill: "#54c2a2",
                          },
                        }}
                      ></CircularProgressbarWithChildren>
                    </div>
                    <div style={{ width: 60, height: 60 }}>
                      <span className="title">REMAINING BDG</span>
                      <CircularProgressbarWithChildren
                        value={RemainingBudget}
                        text={`${RemainingBudget}%`}
                        styles={{
                          path: {
                            stroke: "#331a73",
                            strokeLinecap: "butt",
                            transition: "stroke-dashoffset 0.5s ease 0s",
                            transformOrigin: "center center",
                          },
                          trail: {
                            stroke: "#a653e6",
                            strokeLinecap: "butt",
                            transform: "rotate(0.25turn)",
                            transformOrigin: "center center",
                          },
                          text: {
                            fill: "black",
                            fontSize: "20px",
                            fontWeight: "700",
                          },
                          background: {
                            fill: "#54c2a2",
                          },
                        }}
                      ></CircularProgressbarWithChildren>
                    </div>
                    <div style={{ width: 60, height: 60 }}>
                      <span className="title">RISK</span>
                      <CircularProgressbarWithChildren
                        value={Risk}
                        text={`${Risk}%`}
                        styles={{
                          path: {
                            stroke: "#f7c56d",
                            strokeLinecap: "butt",
                            transition: "stroke-dashoffset 0.5s ease 0s",
                            transformOrigin: "center center",
                          },
                          trail: {
                            stroke: "#e6d4b5",
                            strokeLinecap: "butt",
                            transform: "rotate(0.25turn)",
                            transformOrigin: "center center",
                          },
                          text: {
                            fill: "black",
                            fontSize: "20px",
                            fontWeight: "700",
                          },
                          background: {
                            fill: "#54c2a2",
                          },
                        }}
                      ></CircularProgressbarWithChildren>
                    </div>
                  </div>
                </div>
                <div className="card_container">
                  <img
                    src={Project}
                    alt="Avatar"
                    style={{
                      width: "100%",
                      height: "55%",
                      borderTopLeftRadius: "20px",
                      borderTopRightRadius: "20px",
                    }}
                  />
                  <div className="container_box">
                    <div style={{ width: 60, height: 60 }}>
                      <span className="title">PROJECT STATS</span>
                      <CircularProgressbarWithChildren
                        value={ProjectStatus}
                        text={`${ProjectStatus}%`}
                        styles={{
                          path: {
                            stroke: "#54c2a2",
                            strokeLinecap: "butt",
                            transition: "stroke-dashoffset 0.5s ease 0s",
                            transformOrigin: "center center",
                          },
                          trail: {
                            stroke: "#b0ebda",
                            strokeLinecap: "butt",
                            transform: "rotate(0.25turn)",
                            transformOrigin: "center center",
                          },
                          text: {
                            fill: "black",
                            fontSize: "20px",
                            fontWeight: "700",
                          },
                          background: {
                            fill: "#54c2a2",
                          },
                        }}
                      ></CircularProgressbarWithChildren>
                    </div>
                    <div style={{ width: 60, height: 60 }}>
                      <span className="title">TIMELINE</span>
                      <CircularProgressbarWithChildren
                        value={TimeLine}
                        text={`${TimeLine}%`}
                        styles={{
                          path: {
                            stroke: "#4ccee8",
                            strokeLinecap: "butt",
                            transition: "stroke-dashoffset 0.5s ease 0s",
                            transformOrigin: "center center",
                          },
                          trail: {
                            stroke: "#bbe5ed",
                            strokeLinecap: "butt",
                            transform: "rotate(0.25turn)",
                            transformOrigin: "center center",
                          },
                          text: {
                            fill: "black",
                            fontSize: "20px",
                            fontWeight: "700",
                          },
                          background: {
                            fill: "#54c2a2",
                          },
                        }}
                      ></CircularProgressbarWithChildren>
                    </div>
                    <div style={{ width: 60, height: 60 }}>
                      <span className="title">REMAINING BDG</span>
                      <CircularProgressbarWithChildren
                        value={RemainingBudget}
                        text={`${RemainingBudget}%`}
                        styles={{
                          path: {
                            stroke: "#331a73",
                            strokeLinecap: "butt",
                            transition: "stroke-dashoffset 0.5s ease 0s",
                            transformOrigin: "center center",
                          },
                          trail: {
                            stroke: "#a653e6",
                            strokeLinecap: "butt",
                            transform: "rotate(0.25turn)",
                            transformOrigin: "center center",
                          },
                          text: {
                            fill: "black",
                            fontSize: "20px",
                            fontWeight: "700",
                          },
                          background: {
                            fill: "#54c2a2",
                          },
                        }}
                      ></CircularProgressbarWithChildren>
                    </div>
                    <div style={{ width: 60, height: 60 }}>
                      <span className="title">RISK</span>
                      <CircularProgressbarWithChildren
                        value={Risk}
                        text={`${Risk}%`}
                        styles={{
                          path: {
                            stroke: "#f7c56d",
                            strokeLinecap: "butt",
                            transition: "stroke-dashoffset 0.5s ease 0s",
                            transformOrigin: "center center",
                          },
                          trail: {
                            stroke: "#e6d4b5",
                            strokeLinecap: "butt",
                            transform: "rotate(0.25turn)",
                            transformOrigin: "center center",
                          },
                          text: {
                            fill: "black",
                            fontSize: "20px",
                            fontWeight: "700",
                          },
                          background: {
                            fill: "#54c2a2",
                          },
                        }}
                      ></CircularProgressbarWithChildren>
                    </div>
                  </div>
                </div>
                <div className="card_container">
                  <img
                    src={Project}
                    alt="Avatar"
                    style={{
                      width: "100%",
                      height: "55%",
                      borderTopLeftRadius: "20px",
                      borderTopRightRadius: "20px",
                    }}
                  />
                  <div className="container_box">
                    <div style={{ width: 60, height: 60 }}>
                      <span className="title">PROJECT STATS</span>
                      <CircularProgressbarWithChildren
                        value={ProjectStatus}
                        text={`${ProjectStatus}%`}
                        styles={{
                          path: {
                            stroke: "#54c2a2",
                            strokeLinecap: "butt",
                            transition: "stroke-dashoffset 0.5s ease 0s",
                            transformOrigin: "center center",
                          },
                          trail: {
                            stroke: "#b0ebda",
                            strokeLinecap: "butt",
                            transform: "rotate(0.25turn)",
                            transformOrigin: "center center",
                          },
                          text: {
                            fill: "black",
                            fontSize: "20px",
                            fontWeight: "700",
                          },
                          background: {
                            fill: "#54c2a2",
                          },
                        }}
                      ></CircularProgressbarWithChildren>
                    </div>
                    <div style={{ width: 60, height: 60 }}>
                      <span className="title">TIMELINE</span>
                      <CircularProgressbarWithChildren
                        value={TimeLine}
                        text={`${TimeLine}%`}
                        styles={{
                          path: {
                            stroke: "#4ccee8",
                            strokeLinecap: "butt",
                            transition: "stroke-dashoffset 0.5s ease 0s",
                            transformOrigin: "center center",
                          },
                          trail: {
                            stroke: "#bbe5ed",
                            strokeLinecap: "butt",
                            transform: "rotate(0.25turn)",
                            transformOrigin: "center center",
                          },
                          text: {
                            fill: "black",
                            fontSize: "20px",
                            fontWeight: "700",
                          },
                          background: {
                            fill: "#54c2a2",
                          },
                        }}
                      ></CircularProgressbarWithChildren>
                    </div>
                    <div style={{ width: 60, height: 60 }}>
                      <span className="title">REMAINING BDG</span>
                      <CircularProgressbarWithChildren
                        value={RemainingBudget}
                        text={`${RemainingBudget}%`}
                        styles={{
                          path: {
                            stroke: "#331a73",
                            strokeLinecap: "butt",
                            transition: "stroke-dashoffset 0.5s ease 0s",
                            transformOrigin: "center center",
                          },
                          trail: {
                            stroke: "#a653e6",
                            strokeLinecap: "butt",
                            transform: "rotate(0.25turn)",
                            transformOrigin: "center center",
                          },
                          text: {
                            fill: "black",
                            fontSize: "20px",
                            fontWeight: "700",
                          },
                          background: {
                            fill: "#54c2a2",
                          },
                        }}
                      ></CircularProgressbarWithChildren>
                    </div>
                    <div style={{ width: 60, height: 60 }}>
                      <span className="title">RISK</span>
                      <CircularProgressbarWithChildren
                        value={Risk}
                        text={`${Risk}%`}
                        styles={{
                          path: {
                            stroke: "#f7c56d",
                            strokeLinecap: "butt",
                            transition: "stroke-dashoffset 0.5s ease 0s",
                            transformOrigin: "center center",
                          },
                          trail: {
                            stroke: "#e6d4b5",
                            strokeLinecap: "butt",
                            transform: "rotate(0.25turn)",
                            transformOrigin: "center center",
                          },
                          text: {
                            fill: "black",
                            fontSize: "20px",
                            fontWeight: "700",
                          },
                          background: {
                            fill: "#54c2a2",
                          },
                        }}
                      ></CircularProgressbarWithChildren>
                    </div>
                  </div>
                </div>
                <div className="card_container">
                  <img
                    src={Project}
                    alt="Avatar"
                    style={{
                      width: "100%",
                      height: "55%",
                      borderTopLeftRadius: "20px",
                      borderTopRightRadius: "20px",
                    }}
                  />
                  <div className="container_box">
                    <div style={{ width: 60, height: 60 }}>
                      <span className="title">PROJECT STATS</span>
                      <CircularProgressbarWithChildren
                        value={ProjectStatus}
                        text={`${ProjectStatus}%`}
                        styles={{
                          path: {
                            stroke: "#54c2a2",
                            strokeLinecap: "butt",
                            transition: "stroke-dashoffset 0.5s ease 0s",
                            transformOrigin: "center center",
                          },
                          trail: {
                            stroke: "#b0ebda",
                            strokeLinecap: "butt",
                            transform: "rotate(0.25turn)",
                            transformOrigin: "center center",
                          },
                          text: {
                            fill: "black",
                            fontSize: "20px",
                            fontWeight: "700",
                          },
                          background: {
                            fill: "#54c2a2",
                          },
                        }}
                      ></CircularProgressbarWithChildren>
                    </div>
                    <div style={{ width: 60, height: 60 }}>
                      <span className="title">TIMELINE</span>
                      <CircularProgressbarWithChildren
                        value={TimeLine}
                        text={`${TimeLine}%`}
                        styles={{
                          path: {
                            stroke: "#4ccee8",
                            strokeLinecap: "butt",
                            transition: "stroke-dashoffset 0.5s ease 0s",
                            transformOrigin: "center center",
                          },
                          trail: {
                            stroke: "#bbe5ed",
                            strokeLinecap: "butt",
                            transform: "rotate(0.25turn)",
                            transformOrigin: "center center",
                          },
                          text: {
                            fill: "black",
                            fontSize: "20px",
                            fontWeight: "700",
                          },
                          background: {
                            fill: "#54c2a2",
                          },
                        }}
                      ></CircularProgressbarWithChildren>
                    </div>
                    <div style={{ width: 60, height: 60 }}>
                      <span className="title">REMAINING BDG</span>
                      <CircularProgressbarWithChildren
                        value={RemainingBudget}
                        text={`${RemainingBudget}%`}
                        styles={{
                          path: {
                            stroke: "#331a73",
                            strokeLinecap: "butt",
                            transition: "stroke-dashoffset 0.5s ease 0s",
                            transformOrigin: "center center",
                          },
                          trail: {
                            stroke: "#a653e6",
                            strokeLinecap: "butt",
                            transform: "rotate(0.25turn)",
                            transformOrigin: "center center",
                          },
                          text: {
                            fill: "black",
                            fontSize: "20px",
                            fontWeight: "700",
                          },
                          background: {
                            fill: "#54c2a2",
                          },
                        }}
                      ></CircularProgressbarWithChildren>
                    </div>
                    <div style={{ width: 60, height: 60 }}>
                      <span className="title">RISK</span>
                      <CircularProgressbarWithChildren
                        value={Risk}
                        text={`${Risk}%`}
                        styles={{
                          path: {
                            stroke: "#f7c56d",
                            strokeLinecap: "butt",
                            transition: "stroke-dashoffset 0.5s ease 0s",
                            transformOrigin: "center center",
                          },
                          trail: {
                            stroke: "#e6d4b5",
                            strokeLinecap: "butt",
                            transform: "rotate(0.25turn)",
                            transformOrigin: "center center",
                          },
                          text: {
                            fill: "black",
                            fontSize: "20px",
                            fontWeight: "700",
                          },
                          background: {
                            fill: "#54c2a2",
                          },
                        }}
                      ></CircularProgressbarWithChildren>
                    </div>
                  </div>
                </div>
                <div className="card_container">
                  <img
                    src={Project}
                    alt="Avatar"
                    style={{
                      width: "100%",
                      height: "55%",
                      borderTopLeftRadius: "20px",
                      borderTopRightRadius: "20px",
                    }}
                  />
                  <div className="container_box">
                    <div style={{ width: 60, height: 60 }}>
                      <span className="title">PROJECT STATS</span>
                      <CircularProgressbarWithChildren
                        value={ProjectStatus}
                        text={`${ProjectStatus}%`}
                        styles={{
                          path: {
                            stroke: "#54c2a2",
                            strokeLinecap: "butt",
                            transition: "stroke-dashoffset 0.5s ease 0s",
                            transformOrigin: "center center",
                          },
                          trail: {
                            stroke: "#b0ebda",
                            strokeLinecap: "butt",
                            transform: "rotate(0.25turn)",
                            transformOrigin: "center center",
                          },
                          text: {
                            fill: "black",
                            fontSize: "20px",
                            fontWeight: "700",
                          },
                          background: {
                            fill: "#54c2a2",
                          },
                        }}
                      ></CircularProgressbarWithChildren>
                    </div>
                    <div style={{ width: 60, height: 60 }}>
                      <span className="title">TIMELINE</span>
                      <CircularProgressbarWithChildren
                        value={TimeLine}
                        text={`${TimeLine}%`}
                        styles={{
                          path: {
                            stroke: "#4ccee8",
                            strokeLinecap: "butt",
                            transition: "stroke-dashoffset 0.5s ease 0s",
                            transformOrigin: "center center",
                          },
                          trail: {
                            stroke: "#bbe5ed",
                            strokeLinecap: "butt",
                            transform: "rotate(0.25turn)",
                            transformOrigin: "center center",
                          },
                          text: {
                            fill: "black",
                            fontSize: "20px",
                            fontWeight: "700",
                          },
                          background: {
                            fill: "#54c2a2",
                          },
                        }}
                      ></CircularProgressbarWithChildren>
                    </div>
                    <div style={{ width: 60, height: 60 }}>
                      <span className="title">REMAINING BDG</span>
                      <CircularProgressbarWithChildren
                        value={RemainingBudget}
                        text={`${RemainingBudget}%`}
                        styles={{
                          path: {
                            stroke: "#331a73",
                            strokeLinecap: "butt",
                            transition: "stroke-dashoffset 0.5s ease 0s",
                            transformOrigin: "center center",
                          },
                          trail: {
                            stroke: "#a653e6",
                            strokeLinecap: "butt",
                            transform: "rotate(0.25turn)",
                            transformOrigin: "center center",
                          },
                          text: {
                            fill: "black",
                            fontSize: "20px",
                            fontWeight: "700",
                          },
                          background: {
                            fill: "#54c2a2",
                          },
                        }}
                      ></CircularProgressbarWithChildren>
                    </div>
                    <div style={{ width: 60, height: 60 }}>
                      <span className="title">RISK</span>
                      <CircularProgressbarWithChildren
                        value={Risk}
                        text={`${Risk}%`}
                        styles={{
                          path: {
                            stroke: "#f7c56d",
                            strokeLinecap: "butt",
                            transition: "stroke-dashoffset 0.5s ease 0s",
                            transformOrigin: "center center",
                          },
                          trail: {
                            stroke: "#e6d4b5",
                            strokeLinecap: "butt",
                            transform: "rotate(0.25turn)",
                            transformOrigin: "center center",
                          },
                          text: {
                            fill: "black",
                            fontSize: "20px",
                            fontWeight: "700",
                          },
                          background: {
                            fill: "#54c2a2",
                          },
                        }}
                      ></CircularProgressbarWithChildren>
                    </div>
                  </div>
                </div>
              </Carousel>
            </CCardBody>
          </CCard>
          <CCard>
            <CCardHeader className="text-center">
              <h2>BOTTOM 5 EARNING PROJECTS</h2>
            </CCardHeader>
            <CCardBody>
              {/* <div style={{ marginBottom: "10px" }}>
                <span className="badge_green">PROJECT STATUS</span>
                <span className="badge_blue">TINELINE</span>
                <span className="badge_violet">REMAINING BUDGET</span>
                <span className="badge_orange">RISK</span>
              </div> */}
              <Carousel responsive={responsive}>
                <div className="card_container">
                  <img
                    src={Project}
                    alt="Avatar"
                    style={{
                      width: "100%",
                      height: "55%",
                      borderTopLeftRadius: "20px",
                      borderTopRightRadius: "20px",
                    }}
                  />
                  <div className="container_box">
                    <div style={{ width: 60, height: 60 }}>
                      <span className="title">PROJECT STATS</span>
                      <CircularProgressbarWithChildren
                        value={ProjectStatus}
                        text={`${ProjectStatus}%`}
                        styles={{
                          path: {
                            stroke: "#54c2a2",
                            strokeLinecap: "butt",
                            transition: "stroke-dashoffset 0.5s ease 0s",
                            transformOrigin: "center center",
                          },
                          trail: {
                            stroke: "#b0ebda",
                            strokeLinecap: "butt",
                            transform: "rotate(0.25turn)",
                            transformOrigin: "center center",
                          },
                          text: {
                            fill: "black",
                            fontSize: "20px",
                            fontWeight: "700",
                          },
                          background: {
                            fill: "#54c2a2",
                          },
                        }}
                      ></CircularProgressbarWithChildren>
                    </div>
                    <div style={{ width: 60, height: 60 }}>
                      <span className="title">TIMELINE</span>
                      <CircularProgressbarWithChildren
                        value={TimeLine}
                        text={`${TimeLine}%`}
                        styles={{
                          path: {
                            stroke: "#4ccee8",
                            strokeLinecap: "butt",
                            transition: "stroke-dashoffset 0.5s ease 0s",
                            transformOrigin: "center center",
                          },
                          trail: {
                            stroke: "#bbe5ed",
                            strokeLinecap: "butt",
                            transform: "rotate(0.25turn)",
                            transformOrigin: "center center",
                          },
                          text: {
                            fill: "black",
                            fontSize: "20px",
                            fontWeight: "700",
                          },
                          background: {
                            fill: "#54c2a2",
                          },
                        }}
                      ></CircularProgressbarWithChildren>
                    </div>
                    <div style={{ width: 60, height: 60 }}>
                      <span className="title">REMAINING BDG</span>
                      <CircularProgressbarWithChildren
                        value={RemainingBudget}
                        text={`${RemainingBudget}%`}
                        styles={{
                          path: {
                            stroke: "#331a73",
                            strokeLinecap: "butt",
                            transition: "stroke-dashoffset 0.5s ease 0s",
                            transformOrigin: "center center",
                          },
                          trail: {
                            stroke: "#a653e6",
                            strokeLinecap: "butt",
                            transform: "rotate(0.25turn)",
                            transformOrigin: "center center",
                          },
                          text: {
                            fill: "black",
                            fontSize: "20px",
                            fontWeight: "700",
                          },
                          background: {
                            fill: "#54c2a2",
                          },
                        }}
                      ></CircularProgressbarWithChildren>
                    </div>
                    <div style={{ width: 60, height: 60 }}>
                      <span className="title">RISK</span>
                      <CircularProgressbarWithChildren
                        value={Risk}
                        text={`${Risk}%`}
                        styles={{
                          path: {
                            stroke: "#f7c56d",
                            strokeLinecap: "butt",
                            transition: "stroke-dashoffset 0.5s ease 0s",
                            transformOrigin: "center center",
                          },
                          trail: {
                            stroke: "#e6d4b5",
                            strokeLinecap: "butt",
                            transform: "rotate(0.25turn)",
                            transformOrigin: "center center",
                          },
                          text: {
                            fill: "black",
                            fontSize: "20px",
                            fontWeight: "700",
                          },
                          background: {
                            fill: "#54c2a2",
                          },
                        }}
                      ></CircularProgressbarWithChildren>
                    </div>
                  </div>
                </div>
                <div className="card_container">
                  <img
                    src={Project}
                    alt="Avatar"
                    style={{
                      width: "100%",
                      height: "55%",
                      borderTopLeftRadius: "20px",
                      borderTopRightRadius: "20px",
                    }}
                  />
                  <div className="container_box">
                    <div style={{ width: 60, height: 60 }}>
                      <span className="title">PROJECT STATS</span>
                      <CircularProgressbarWithChildren
                        value={ProjectStatus}
                        text={`${ProjectStatus}%`}
                        styles={{
                          path: {
                            stroke: "#54c2a2",
                            strokeLinecap: "butt",
                            transition: "stroke-dashoffset 0.5s ease 0s",
                            transformOrigin: "center center",
                          },
                          trail: {
                            stroke: "#b0ebda",
                            strokeLinecap: "butt",
                            transform: "rotate(0.25turn)",
                            transformOrigin: "center center",
                          },
                          text: {
                            fill: "black",
                            fontSize: "20px",
                            fontWeight: "700",
                          },
                          background: {
                            fill: "#54c2a2",
                          },
                        }}
                      ></CircularProgressbarWithChildren>
                    </div>
                    <div style={{ width: 60, height: 60 }}>
                      <span className="title">TIMELINE</span>
                      <CircularProgressbarWithChildren
                        value={TimeLine}
                        text={`${TimeLine}%`}
                        styles={{
                          path: {
                            stroke: "#4ccee8",
                            strokeLinecap: "butt",
                            transition: "stroke-dashoffset 0.5s ease 0s",
                            transformOrigin: "center center",
                          },
                          trail: {
                            stroke: "#bbe5ed",
                            strokeLinecap: "butt",
                            transform: "rotate(0.25turn)",
                            transformOrigin: "center center",
                          },
                          text: {
                            fill: "black",
                            fontSize: "20px",
                            fontWeight: "700",
                          },
                          background: {
                            fill: "#54c2a2",
                          },
                        }}
                      ></CircularProgressbarWithChildren>
                    </div>
                    <div style={{ width: 60, height: 60 }}>
                      <span className="title">REMAINING BDG</span>
                      <CircularProgressbarWithChildren
                        value={RemainingBudget}
                        text={`${RemainingBudget}%`}
                        styles={{
                          path: {
                            stroke: "#331a73",
                            strokeLinecap: "butt",
                            transition: "stroke-dashoffset 0.5s ease 0s",
                            transformOrigin: "center center",
                          },
                          trail: {
                            stroke: "#a653e6",
                            strokeLinecap: "butt",
                            transform: "rotate(0.25turn)",
                            transformOrigin: "center center",
                          },
                          text: {
                            fill: "black",
                            fontSize: "20px",
                            fontWeight: "700",
                          },
                          background: {
                            fill: "#54c2a2",
                          },
                        }}
                      ></CircularProgressbarWithChildren>
                    </div>
                    <div style={{ width: 60, height: 60 }}>
                      <span className="title">RISK</span>
                      <CircularProgressbarWithChildren
                        value={Risk}
                        text={`${Risk}%`}
                        styles={{
                          path: {
                            stroke: "#f7c56d",
                            strokeLinecap: "butt",
                            transition: "stroke-dashoffset 0.5s ease 0s",
                            transformOrigin: "center center",
                          },
                          trail: {
                            stroke: "#e6d4b5",
                            strokeLinecap: "butt",
                            transform: "rotate(0.25turn)",
                            transformOrigin: "center center",
                          },
                          text: {
                            fill: "black",
                            fontSize: "20px",
                            fontWeight: "700",
                          },
                          background: {
                            fill: "#54c2a2",
                          },
                        }}
                      ></CircularProgressbarWithChildren>
                    </div>
                  </div>
                </div>
                <div className="card_container">
                  <img
                    src={Project}
                    alt="Avatar"
                    style={{
                      width: "100%",
                      height: "55%",
                      borderTopLeftRadius: "20px",
                      borderTopRightRadius: "20px",
                    }}
                  />
                  <div className="container_box">
                    <div style={{ width: 60, height: 60 }}>
                      <span className="title">PROJECT STATS</span>
                      <CircularProgressbarWithChildren
                        value={ProjectStatus}
                        text={`${ProjectStatus}%`}
                        styles={{
                          path: {
                            stroke: "#54c2a2",
                            strokeLinecap: "butt",
                            transition: "stroke-dashoffset 0.5s ease 0s",
                            transformOrigin: "center center",
                          },
                          trail: {
                            stroke: "#b0ebda",
                            strokeLinecap: "butt",
                            transform: "rotate(0.25turn)",
                            transformOrigin: "center center",
                          },
                          text: {
                            fill: "black",
                            fontSize: "20px",
                            fontWeight: "700",
                          },
                          background: {
                            fill: "#54c2a2",
                          },
                        }}
                      ></CircularProgressbarWithChildren>
                    </div>
                    <div style={{ width: 60, height: 60 }}>
                      <span className="title">TIMELINE</span>
                      <CircularProgressbarWithChildren
                        value={TimeLine}
                        text={`${TimeLine}%`}
                        styles={{
                          path: {
                            stroke: "#4ccee8",
                            strokeLinecap: "butt",
                            transition: "stroke-dashoffset 0.5s ease 0s",
                            transformOrigin: "center center",
                          },
                          trail: {
                            stroke: "#bbe5ed",
                            strokeLinecap: "butt",
                            transform: "rotate(0.25turn)",
                            transformOrigin: "center center",
                          },
                          text: {
                            fill: "black",
                            fontSize: "20px",
                            fontWeight: "700",
                          },
                          background: {
                            fill: "#54c2a2",
                          },
                        }}
                      ></CircularProgressbarWithChildren>
                    </div>
                    <div style={{ width: 60, height: 60 }}>
                      <span className="title">REMAINING BDG</span>
                      <CircularProgressbarWithChildren
                        value={RemainingBudget}
                        text={`${RemainingBudget}%`}
                        styles={{
                          path: {
                            stroke: "#331a73",
                            strokeLinecap: "butt",
                            transition: "stroke-dashoffset 0.5s ease 0s",
                            transformOrigin: "center center",
                          },
                          trail: {
                            stroke: "#a653e6",
                            strokeLinecap: "butt",
                            transform: "rotate(0.25turn)",
                            transformOrigin: "center center",
                          },
                          text: {
                            fill: "black",
                            fontSize: "20px",
                            fontWeight: "700",
                          },
                          background: {
                            fill: "#54c2a2",
                          },
                        }}
                      ></CircularProgressbarWithChildren>
                    </div>
                    <div style={{ width: 60, height: 60 }}>
                      <span className="title">RISK</span>
                      <CircularProgressbarWithChildren
                        value={Risk}
                        text={`${Risk}%`}
                        styles={{
                          path: {
                            stroke: "#f7c56d",
                            strokeLinecap: "butt",
                            transition: "stroke-dashoffset 0.5s ease 0s",
                            transformOrigin: "center center",
                          },
                          trail: {
                            stroke: "#e6d4b5",
                            strokeLinecap: "butt",
                            transform: "rotate(0.25turn)",
                            transformOrigin: "center center",
                          },
                          text: {
                            fill: "black",
                            fontSize: "20px",
                            fontWeight: "700",
                          },
                          background: {
                            fill: "#54c2a2",
                          },
                        }}
                      ></CircularProgressbarWithChildren>
                    </div>
                  </div>
                </div>
                <div className="card_container">
                  <img
                    src={Project}
                    alt="Avatar"
                    style={{
                      width: "100%",
                      height: "55%",
                      borderTopLeftRadius: "20px",
                      borderTopRightRadius: "20px",
                    }}
                  />
                  <div className="container_box">
                    <div style={{ width: 60, height: 60 }}>
                      <span className="title">PROJECT STATS</span>
                      <CircularProgressbarWithChildren
                        value={ProjectStatus}
                        text={`${ProjectStatus}%`}
                        styles={{
                          path: {
                            stroke: "#54c2a2",
                            strokeLinecap: "butt",
                            transition: "stroke-dashoffset 0.5s ease 0s",
                            transformOrigin: "center center",
                          },
                          trail: {
                            stroke: "#b0ebda",
                            strokeLinecap: "butt",
                            transform: "rotate(0.25turn)",
                            transformOrigin: "center center",
                          },
                          text: {
                            fill: "black",
                            fontSize: "20px",
                            fontWeight: "700",
                          },
                          background: {
                            fill: "#54c2a2",
                          },
                        }}
                      ></CircularProgressbarWithChildren>
                    </div>
                    <div style={{ width: 60, height: 60 }}>
                      <span className="title">TIMELINE</span>
                      <CircularProgressbarWithChildren
                        value={TimeLine}
                        text={`${TimeLine}%`}
                        styles={{
                          path: {
                            stroke: "#4ccee8",
                            strokeLinecap: "butt",
                            transition: "stroke-dashoffset 0.5s ease 0s",
                            transformOrigin: "center center",
                          },
                          trail: {
                            stroke: "#bbe5ed",
                            strokeLinecap: "butt",
                            transform: "rotate(0.25turn)",
                            transformOrigin: "center center",
                          },
                          text: {
                            fill: "black",
                            fontSize: "20px",
                            fontWeight: "700",
                          },
                          background: {
                            fill: "#54c2a2",
                          },
                        }}
                      ></CircularProgressbarWithChildren>
                    </div>
                    <div style={{ width: 60, height: 60 }}>
                      <span className="title">REMAINING BDG</span>
                      <CircularProgressbarWithChildren
                        value={RemainingBudget}
                        text={`${RemainingBudget}%`}
                        styles={{
                          path: {
                            stroke: "#331a73",
                            strokeLinecap: "butt",
                            transition: "stroke-dashoffset 0.5s ease 0s",
                            transformOrigin: "center center",
                          },
                          trail: {
                            stroke: "#a653e6",
                            strokeLinecap: "butt",
                            transform: "rotate(0.25turn)",
                            transformOrigin: "center center",
                          },
                          text: {
                            fill: "black",
                            fontSize: "20px",
                            fontWeight: "700",
                          },
                          background: {
                            fill: "#54c2a2",
                          },
                        }}
                      ></CircularProgressbarWithChildren>
                    </div>
                    <div style={{ width: 60, height: 60 }}>
                      <span className="title">RISK</span>
                      <CircularProgressbarWithChildren
                        value={Risk}
                        text={`${Risk}%`}
                        styles={{
                          path: {
                            stroke: "#f7c56d",
                            strokeLinecap: "butt",
                            transition: "stroke-dashoffset 0.5s ease 0s",
                            transformOrigin: "center center",
                          },
                          trail: {
                            stroke: "#e6d4b5",
                            strokeLinecap: "butt",
                            transform: "rotate(0.25turn)",
                            transformOrigin: "center center",
                          },
                          text: {
                            fill: "black",
                            fontSize: "20px",
                            fontWeight: "700",
                          },
                          background: {
                            fill: "#54c2a2",
                          },
                        }}
                      ></CircularProgressbarWithChildren>
                    </div>
                  </div>
                </div>
                <div className="card_container">
                  <img
                    src={Project}
                    alt="Avatar"
                    style={{
                      width: "100%",
                      height: "55%",
                      borderTopLeftRadius: "20px",
                      borderTopRightRadius: "20px",
                    }}
                  />
                  <div className="container_box">
                    <div style={{ width: 60, height: 60 }}>
                      <span className="title">PROJECT STATS</span>
                      <CircularProgressbarWithChildren
                        value={ProjectStatus}
                        text={`${ProjectStatus}%`}
                        styles={{
                          path: {
                            stroke: "#54c2a2",
                            strokeLinecap: "butt",
                            transition: "stroke-dashoffset 0.5s ease 0s",
                            transformOrigin: "center center",
                          },
                          trail: {
                            stroke: "#b0ebda",
                            strokeLinecap: "butt",
                            transform: "rotate(0.25turn)",
                            transformOrigin: "center center",
                          },
                          text: {
                            fill: "black",
                            fontSize: "20px",
                            fontWeight: "700",
                          },
                          background: {
                            fill: "#54c2a2",
                          },
                        }}
                      ></CircularProgressbarWithChildren>
                    </div>
                    <div style={{ width: 60, height: 60 }}>
                      <span className="title">TIMELINE</span>
                      <CircularProgressbarWithChildren
                        value={TimeLine}
                        text={`${TimeLine}%`}
                        styles={{
                          path: {
                            stroke: "#4ccee8",
                            strokeLinecap: "butt",
                            transition: "stroke-dashoffset 0.5s ease 0s",
                            transformOrigin: "center center",
                          },
                          trail: {
                            stroke: "#bbe5ed",
                            strokeLinecap: "butt",
                            transform: "rotate(0.25turn)",
                            transformOrigin: "center center",
                          },
                          text: {
                            fill: "black",
                            fontSize: "20px",
                            fontWeight: "700",
                          },
                          background: {
                            fill: "#54c2a2",
                          },
                        }}
                      ></CircularProgressbarWithChildren>
                    </div>
                    <div style={{ width: 60, height: 60 }}>
                      <span className="title">REMAINING BDG</span>
                      <CircularProgressbarWithChildren
                        value={RemainingBudget}
                        text={`${RemainingBudget}%`}
                        styles={{
                          path: {
                            stroke: "#331a73",
                            strokeLinecap: "butt",
                            transition: "stroke-dashoffset 0.5s ease 0s",
                            transformOrigin: "center center",
                          },
                          trail: {
                            stroke: "#a653e6",
                            strokeLinecap: "butt",
                            transform: "rotate(0.25turn)",
                            transformOrigin: "center center",
                          },
                          text: {
                            fill: "black",
                            fontSize: "20px",
                            fontWeight: "700",
                          },
                          background: {
                            fill: "#54c2a2",
                          },
                        }}
                      ></CircularProgressbarWithChildren>
                    </div>
                    <div style={{ width: 60, height: 60 }}>
                      <span className="title">RISK</span>
                      <CircularProgressbarWithChildren
                        value={Risk}
                        text={`${Risk}%`}
                        styles={{
                          path: {
                            stroke: "#f7c56d",
                            strokeLinecap: "butt",
                            transition: "stroke-dashoffset 0.5s ease 0s",
                            transformOrigin: "center center",
                          },
                          trail: {
                            stroke: "#e6d4b5",
                            strokeLinecap: "butt",
                            transform: "rotate(0.25turn)",
                            transformOrigin: "center center",
                          },
                          text: {
                            fill: "black",
                            fontSize: "20px",
                            fontWeight: "700",
                          },
                          background: {
                            fill: "#54c2a2",
                          },
                        }}
                      ></CircularProgressbarWithChildren>
                    </div>
                  </div>
                </div>
              </Carousel>
            </CCardBody>
          </CCard>
        </CCol>
        <CCol sm="12" md="3">
          <CCard>
            <CCardHeader>
              <h4>SEB Projects (Ranked)</h4>
            </CCardHeader>
            <CCardBody>
              <div className="list_container">{TopList}</div>
            </CCardBody>
            <CCardFooter>
              <div className="list_footer">{BottomList}</div>
            </CCardFooter>
          </CCard>
        </CCol>
      </CRow>
    </>
  );
};

export default Dashboard;
