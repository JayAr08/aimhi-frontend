const dupa = [
  {
    project_name:
      "Construction of Multi-Purpose Bldg. (Senior Citizen), Brgy. Lapu-Lapu, Maco, Compostella Valley",
    location: "Maco, Davao De Oro",
    headers: [
      "Description",
      "qty",
      "unit",
      "unit price",
      "estimated cost",
      // "actual cost",
      // "difference",
    ],
    scope: [
      {
        0: "Materials",
        value: [
          {
            0: "Sanitary water closet flush",
            1: "4",
            2: "sets",
            3: 2000.0,
            4: 20000.0,
            5: 0,
            6: "0",
          },
          {
            0: "Lavatory Including fittings",
            1: "4",
            2: "sets",
            3: 3500.0,
            4: 14000.0,
            5: 0,
            6: 0,
          },
          {
            0: "Water meter",
            1: "1",
            2: "pc",
            3: 1600.0,
            4: 1600.0,
            5: 0,
            6: 0,
          },
          {
            0: "Foldable stainless grab rail",
            1: "1",
            2: "lgth",
            3: 4800.0,
            4: 4800.0,
            6: 0,
            7: 0,
          },
        ],
      },
      {
        0: "Equipment",
        value: [
          {
            0: "Minor Tools(105 of Labor Cost)",
            1: "",
            2: "",
            3: 0,
            4: 1320.32,
            5: 0,
            6: "0",
          },
        ],
      },
      {
        0: "Permet and clearances",
        value: [
          {
            0: "Permet and clearances 2",
            1: "1",
            2: "Is.",
            3: 10000.0,
            4: 5000.0,
            5: 5000.0,
            6: "0",
          },
          {
            0: "Permet and clearances 2",
            1: "1",
            2: "Is.",
            3: 5000.0,
            4: 5000.0,
            5: 5000.0,
            6: "0",
          },
        ],
      },
    ],
  },
];

export default dupa;
